$Import =Import-CSV "users.csv"
$OU = "OU=LabInternal,DC=labs,DC=local" # Choose appropriate OU!!
 
Foreach ($user in $Import)
{ 
	$password = $user.Password | ConvertTo-SecureString -AsPlainText -Force
	New-ADUser -Name $user.Name -GivenName $user.FirstName -Surname $user.LastName -Path $OU -AccountPassword $Password -ChangePasswordAtLogon $False -Enabled $True
}