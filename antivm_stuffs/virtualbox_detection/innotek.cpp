//Source: //http://waleedassar.blogspot.com (@waleedassar)
//MS VC++ 2005 + DirectX SDK
#include "stdafx.h"
#include "dxdiag.h"
#include "stdio.h"

int main(int argc, _TCHAR* argv[])
{
    HRESULT hr=CoInitialize(0);
	if(!SUCCEEDED(hr)) return 0;
	IDxDiagProvider* pProvider = NULL;
    hr=CoCreateInstance(CLSID_DxDiagProvider,0,CLSCTX_INPROC_SERVER,IID_IDxDiagProvider,(void**)&pProvider );
	if(!SUCCEEDED(hr))
	{
		CoUninitialize();
		return 0;
	}
	DXDIAG_INIT_PARAMS InitParams={0};
	InitParams.dwSize=sizeof(DXDIAG_INIT_PARAMS);
	InitParams.dwDxDiagHeaderVersion=DXDIAG_DX9_SDK_VERSION;
	InitParams.bAllowWHQLChecks=false;
    hr=pProvider->Initialize(&InitParams);
    if(SUCCEEDED(hr))
	{
          IDxDiagContainer* pDxDiagRoot=0;
          IDxDiagContainer* pDxDiagSystemInfo=0;
          hr=pProvider->GetRootContainer(&pDxDiagRoot );
          if(SUCCEEDED(hr)) 
          {
                hr=pDxDiagRoot->GetChildContainer( L"DxDiag_SystemInfo", &pDxDiagSystemInfo );
                if(SUCCEEDED(hr) )
                {
                       VARIANT varX;
					   hr=pDxDiagSystemInfo->GetProp( L"szSystemManufacturerEnglish",&varX);
                       if( SUCCEEDED(hr)&&varX.vt==VT_BSTR && SysStringLen(varX.bstrVal)!=0)
                       {
						   wchar_t* pMan=varX.bstrVal;
                           wprintf(L"System Manufacturer is %s\r\n",pMan);
						   if(!_wcsicmp(pMan,L"innotek GmbH"))
						   {
							   printf("VirtualBox detected\r\n");
						   }
				       }
                       VariantClear(&varX);
					   hr=pDxDiagSystemInfo->GetProp( L"szSystemModelEnglish",&varX);
                       if( SUCCEEDED(hr)&&varX.vt==VT_BSTR && SysStringLen(varX.bstrVal)!=0)
                       {
						   wchar_t* pMan=varX.bstrVal;
                           wprintf(L"System Model is %s\r\n",pMan);
						   if(!_wcsicmp(pMan,L"VirtualBox"))
						   {
							   printf("VirtualBox detected\r\n");
						   }
				       }
                       VariantClear(&varX);
                       pDxDiagSystemInfo->Release();
                }
                pDxDiagRoot->Release();
        }
        //pProvider->Release();
    }
    CoUninitialize();
	return 0;
}
