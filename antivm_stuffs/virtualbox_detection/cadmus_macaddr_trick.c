//Source: http://pastebin.com/AjHWApes
//http://waleedassar.blogspot.com (@waleedassar)
//VirtualBox Adapters (Host and Guests) always have their MAC addresses in the form of 08-00-27-??-??-??. This range was originally assigned to Cadmus Computer Systems.
//This might show false positive results, but has not been witnessed so far.
#include "stdafx.h"
#include "winsock2.h"
#include "iphlpapi.h"
#include "ws2tcpip.h"
#include "windows.h"
#include "stdio.h"


int main(int argc, char* argv[])
{
	WSADATA WSD;
	if(!WSAStartup(MAKEWORD(2,2),&WSD))
	{
		unsigned long tot_size=0;
		int ret=GetAdaptersAddresses(AF_UNSPEC,GAA_FLAG_INCLUDE_PREFIX,0,0,&tot_size);
		if(ret==ERROR_BUFFER_OVERFLOW)
		{
			IP_ADAPTER_ADDRESSES* px=(IP_ADAPTER_ADDRESSES*)LocalAlloc(LMEM_ZEROINIT,tot_size);
			if(px)
			{
				ret=GetAdaptersAddresses(AF_UNSPEC,GAA_FLAG_INCLUDE_PREFIX,0,px,&tot_size);
				IP_ADAPTER_ADDRESSES* pxx=px;
				//Traverse a singly-linked list
				for(pxx;pxx;pxx=pxx->Next)
				{
					if(pxx->PhysicalAddressLength==0x6)
					{
						if(wcsicmp(pxx->FriendlyName,L"VirtualBox Host-Only Network"))  //We don't want to detect the HOST OS
						{
						    char xx[0x6]={0};
						    memcpy(xx,pxx->PhysicalAddress,0x6);
							if(xx[0]==0x08&& xx[1]==0x00 && xx[2]==0x27) //Cadmus Computer Systems Mac address
							{
								MessageBox(0,L"VirtualBox detected",L"waliedassar",0);
							}
						}
					}
				}
				LocalFree(px);
			}
		}
		WSACleanup();
	}
	ExitProcess(0);
	return 0;
}
