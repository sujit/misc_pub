//Source: http://pastebin.com/Geggzp4G
//http://waleedassar.blogspot.com (@waleedassar)
//Reading "SMBiosData" to extract Bios Brand and Bios Version strings via WMI COM access. If the Bios Brand string is //"innotek GmbH" or Bios Version is "VirtualBox", then it is a sign that we are running in VirtualBox.

#include "stdafx.h"
#include <comdef.h>
#include <Wbemidl.h>
#include "stdio.h"

#pragma comment(lib, "wbemuuid.lib")


void AllToUpper(unsigned char* str,unsigned long len)
{
	for(unsigned long c=0;c<len;c++)
	{
		if(str[c]>='a' && str[c]<='z')
		{
			str[c]-=32;
		}
	}
}

unsigned char* ScanDataForString(unsigned char* data,unsigned long data_length,unsigned char* string2)
{
	unsigned long string_length=(unsigned long)strlen((char*)string2);
	for(unsigned long i=0;i<=(data_length-string_length);i++)
	{
		if(strncmp((char*)(&data[i]),(char*)string2,string_length)==0) return &data[i];
	}
	return 0;
}
int main(int argc, _TCHAR* argv[])
{
	BSTR rootwmi=SysAllocString(L"root\\wmi");
	BSTR tables=SysAllocString(L"MSSmBios_RawSMBiosTables");
    BSTR biosdata=SysAllocString(L"SMBiosData");

	HRESULT hr=CoInitializeEx(0, COINIT_MULTITHREADED);
	if(!SUCCEEDED(hr)) return 0;
	IWbemLocator* pLoc=0;
	hr=CoCreateInstance(CLSID_WbemLocator,0,CLSCTX_INPROC_SERVER,IID_IWbemLocator,(void**)&pLoc);
	if(!SUCCEEDED(hr))
	{
		CoUninitialize();
		return 0;
	}
	IWbemServices* pSvc=0;
	hr=pLoc->ConnectServer(rootwmi,0 ,0 ,0 ,0,0,0,&pSvc);
	if(!SUCCEEDED(hr))
    {
		pLoc->Release();     
        CoUninitialize();
        return 0;
    }
    hr=CoSetProxyBlanket(pSvc,RPC_C_AUTHN_WINNT,RPC_C_AUTHZ_NONE,0,RPC_C_AUTHN_LEVEL_CALL,RPC_C_IMP_LEVEL_IMPERSONATE,0,EOAC_NONE);
	if(!SUCCEEDED(hr))
	{
	    pSvc->Release();
        pLoc->Release();     
        CoUninitialize();
        return 0;
	}

    IEnumWbemClassObject* pEnum=0;
    hr=pSvc->CreateInstanceEnum(tables,0,0, &pEnum);
	if(!SUCCEEDED(hr))
	{
	    pSvc->Release();
        pLoc->Release();     
        CoUninitialize();
        return 0;
	}

	IWbemClassObject* pInstance=0;
    unsigned long Count=0;
    hr=pEnum->Next(WBEM_INFINITE,1,&pInstance,&Count);
	if(SUCCEEDED(hr))
	{		
		 VARIANT BiosData;
		 VariantInit(&BiosData);
		 CIMTYPE type;
		 hr=pInstance->Get(biosdata,0,&BiosData,&type,NULL);
		 if(SUCCEEDED(hr))
		 {
				     SAFEARRAY* p_array = NULL;
				     p_array = V_ARRAY(&BiosData);
					 unsigned char* p_data=(unsigned char *)p_array->pvData;
					 unsigned long length=p_array->rgsabound[0].cElements;
					 AllToUpper(p_data,length);
				     unsigned char* x1=ScanDataForString((unsigned char*)p_data,length,(unsigned char*)"INNOTEK GMBH");
				     unsigned char* x2=ScanDataForString((unsigned char*)p_data,length,(unsigned char*)"VIRTUALBOX");
					 unsigned char* x3=ScanDataForString((unsigned char*)p_data,length,(unsigned char*)"SUN MICROSYSTEMS");
					 unsigned char* x4=ScanDataForString((unsigned char*)p_data,length,(unsigned char*)"VIRTUAL MACHINE");
					 unsigned char* x5=ScanDataForString((unsigned char*)p_data,length,(unsigned char*)"VBOXVER");
					 if(x1 || x2 || x3 || x4 || x5)
					 {
							  printf("VirtualBox detected\r\n");
							  printf("Some Strings found:\r\n");
							  if(x1) printf("%s\r\n",x1);
							  if(x2) printf("%s\r\n",x2);
							  if(x3) printf("%s\r\n",x3);
							  if(x4) printf("%s\r\n",x4);
							  if(x5) printf("%s\r\n",x5);
					}
		 }
		 VariantClear(&BiosData);
		 pInstance->Release();
	}
	pSvc->Release();
    pLoc->Release();     
    CoUninitialize();
    ExitProcess(0);
	return 0;
}
