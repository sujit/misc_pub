//Source: http://pastebin.com/T0s5gVGW
//http://waleedassar.blogspot.com (@waleedassar)
//The following code parses the SMBiosData retrieved from the Windows registry and searches for any structures of TYPE TYPE_INACTIVE (126, 0x7E). This is a sign of VirtualBox existence.
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"

#define TYPE_BIOS 0x0    //e.g. Bios Brand and Version
#define TYPE_SYSTEM 0x1  //System Manufacturer and Model
#define TYPE_BASEBOARD 0x2
#define TYPE_SYSTEM_ENCLOSURE 0x3
#define TYPE_PROCESSOR 0x4
#define TYPE_CACHE_INFO 0x7
#define TYPE_SYSTEM_SLOTS 0x9
#define TYPE_OEM_STRINGS 0xB
#define TYPE_PHYSICAL_MEM_ARRAY 0x10
#define TYPE_MEMORY_DEVICE    0x11
#define TYPE_MEMORY_ARRAY_MAPPED_ADDRESS 0x13
#define TYPE_SYSTEM_BOOT_INFORMATION 0x20
#define TYPE_INACTIVE 0x7E //???? this one
#define TYPE_END_OF_STRUCTURE 0x7F

//----This structure is only need for parsing SMBiosData retrieved from Registry.
//Not needed for parsing SMBiosData retrieved Via WMI
struct BIOS_DATA_HEAD
{
	unsigned char a1;
	unsigned char a2;
	unsigned char a3;
	unsigned char a4;
	unsigned long length;
};

struct HeadER
{
	unsigned char Type;  //0 for bios, 1 for system, and so on.
	unsigned char section_length;
	unsigned short handles;
};

void AllToUpper(char* str,unsigned long len)
{
	for(unsigned long c=0;c<len;c++)
	{
		if(str[c]>='a' && str[c]<='z')
		{
			str[c]-=32;
		}
	}
}

void PrintType(unsigned char type)
{
	 printf("----------------------------------------\r\n");
	 if(type==TYPE_BIOS) printf("Type: BIOS\r\n");
	 else if(type==TYPE_SYSTEM) printf("Type: SYSTEM INFO\r\n");
	 else if(type==TYPE_BASEBOARD) printf("Type: BASEBOARD\r\n");
	 else if(type==TYPE_SYSTEM_ENCLOSURE) printf("Type: BIOS\r\n");
	 else if(type==TYPE_PROCESSOR) printf("Type: PROCESSOR\r\n");
	 else if(type==TYPE_CACHE_INFO) printf("Type: CACHE INFO\r\n");
	 else if(type==TYPE_SYSTEM_SLOTS) printf("Type: SYSTEM SLOTS\r\n");
	 else if(type==TYPE_OEM_STRINGS) printf("Type: OEM STRINGS\r\n");
	 else if(type==TYPE_PHYSICAL_MEM_ARRAY) printf("Type: PHYSICAL MEMORY ARRAY\r\n");
	 else if(type==TYPE_MEMORY_DEVICE) printf("Type: MEMORY DEVICE\r\n");
	 else if(type==TYPE_MEMORY_ARRAY_MAPPED_ADDRESS) printf("Type: MEMORY ARRAY MAPPED ADDRESS\r\n");
	 else if(type==TYPE_SYSTEM_BOOT_INFORMATION) printf("Type: SYSTEM BOOT INFORMATION\r\n");
	 else if(type==TYPE_END_OF_STRUCTURE)   printf("Type: END OF STRUCTURE\r\n");
	 else printf("Type: %X\r\n",type);
}
//index 1 represents the first string
char* PrintString(char* pString,unsigned long index)
{
	index--;
	while(index)
	{
		unsigned long length=strlen(pString);
		pString+=(length+1);
		if(*pString==0)
		{
			printf("String is: Error retrieving string\r\n");
			return 0;
		}
		index--;
	}
	printf("String is: %s\r\n",pString);
	return pString;
}

unsigned char* ScanDataForString(unsigned char* data,unsigned long data_length,unsigned char* string2)
{
	unsigned long string_length=strlen((char*)string2);
	for(unsigned long i=0;i<=(data_length-string_length);i++)
	{
		if(strncmp((char*)(&data[i]),(char*)string2,string_length)==0) return &data[i];
	}
	return 0;
}

int main(int argc, char* argv[])
{
	HKEY hk=0;
	int ret=RegOpenKeyEx(HKEY_LOCAL_MACHINE,"SYSTEM\\CurrentControlSet\\Services\\mssmbios\\data",0,KEY_ALL_ACCESS,&hk);
	if(ret==ERROR_SUCCESS)
	{
		unsigned long type=0;
		unsigned long length=0;
		ret=RegQueryValueEx(hk,"SMBiosData",0,&type,0,&length);
		if(ret==ERROR_SUCCESS)
		{
			if(length)
			{
				char* p=(char*)LocalAlloc(LMEM_ZEROINIT,length);
				if(p)
				{
					ret=RegQueryValueEx(hk,"SMBiosData",0,&type,(unsigned char*)p,&length);
					if(ret==ERROR_SUCCESS)
					{
						//--------------------------Only when parsing SMBiosData retrieved from Registry------------------
						unsigned long new_length=((BIOS_DATA_HEAD*)p)->length;  //length-8
						p+=0x8;
						printf("Length is: %X\r\n",new_length);
						//------------------------------------------------------------------------------------------------
						unsigned long i=0;
						while(i<new_length)
						{
							unsigned char type=((HeadER*)(p+i))->Type;
							PrintType(type);
							unsigned char section_size=((HeadER*)(p+i))->section_length;
							printf("Section length is: %X\r\n",section_size);
							unsigned short handles=((HeadER*)(p+i))->handles;
							printf("Handle is: %X\r\n",handles);

							if(type==0x7F) break; //End-Of-Table

							if(type==TYPE_INACTIVE) //0x7E
							{
							    PrintString(p+i+section_size,*(p+i+4));   //print Brand
							    PrintString(p+i+section_size,*(p+i+5));   //print Version
								MessageBox(0,"VirtualBox detected","waliedassar",0);
							}
							//---Get End of Structure--------------
							unsigned char* pxp=(unsigned char*)p+i+section_size;
							while(*(unsigned short*)pxp!=0) pxp++;
							pxp++;
							pxp++;
							//-------------------------------------
							i=(pxp-((unsigned char*)p));
						}
					}
					LocalFree(p);
				}
			}
		}
		RegCloseKey(hk);
	}
	return 0;
}
