//Source: http://pastebin.com/fPY4MiYq
//http://waleedassar.blogspot.com (@waleedassar)
//Reading "SMBiosData" to extract Bios Brand and Bios Version strings from registry.
//If the Bios Brand string is "innotek GmbH" or Bios Version is "VirtualBox", then it is a sign that we are running in VirtualBox.
//You can also use WMI to extract the same info.
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"


void AllToUpper(char* str,unsigned long len)
{
	for(unsigned long c=0;c<len;c++)
	{
		if(str[c]>='a' && str[c]<='z')
		{
			str[c]-=32;
		}
	}
}

unsigned char* ScanDataForString(unsigned char* data,unsigned long data_length,unsigned char* string2)
{
	unsigned long string_length=strlen((char*)string2);
	for(unsigned long i=0;i<=(data_length-string_length);i++)
	{
		if(strncmp((char*)(&data[i]),(char*)string2,string_length)==0) return &data[i];
	}
	return 0;
}

int main(int argc, char* argv[])
{
	HKEY hk=0;
	int ret=RegOpenKeyEx(HKEY_LOCAL_MACHINE,"SYSTEM\\CurrentControlSet\\Services\\mssmbios\\data",0,KEY_ALL_ACCESS,&hk);
	if(ret==ERROR_SUCCESS)
	{
		unsigned long type=0;
		unsigned long length=0;
		ret=RegQueryValueEx(hk,"SMBiosData",0,&type,0,&length);
		if(ret==ERROR_SUCCESS)
		{
			if(length)
			{
				char* p=(char*)LocalAlloc(LMEM_ZEROINIT,length);
				if(p)
				{
					ret=RegQueryValueEx(hk,"SMBiosData",0,&type,(unsigned char*)p,&length);
					if(ret==ERROR_SUCCESS)
					{
					      AllToUpper(p,length);
						  unsigned char* x1=ScanDataForString((unsigned char*)p,length,(unsigned char*)"INNOTEK GMBH");
						  unsigned char* x2=ScanDataForString((unsigned char*)p,length,(unsigned char*)"VIRTUALBOX");
						  unsigned char* x3=ScanDataForString((unsigned char*)p,length,(unsigned char*)"SUN MICROSYSTEMS");
						  unsigned char* x4=ScanDataForString((unsigned char*)p,length,(unsigned char*)"VIRTUAL MACHINE");
						  unsigned char* x5=ScanDataForString((unsigned char*)p,length,(unsigned char*)"VBOXVER");
						  if(x1 || x2 || x3 || x4 || x5)
						  {
							  printf("VirtualBox detected\r\n");
							  printf("Some Strings found:\r\n");
							  if(x1) printf("%s\r\n",x1);
							  if(x2) printf("%s\r\n",x2);
							  if(x3) printf("%s\r\n",x3);
							  if(x4) printf("%s\r\n",x4);
							  if(x5) printf("%s\r\n",x5);
						  }
					}
					LocalFree(p);
				}
			}
		}
		RegCloseKey(hk);
	}
	return 0;
}
