//Source: http://pastebin.com/Nsv5B1yk
//http://waleedassar.blogspot.com
//http://www.twitter.com/waleedassar
//Use this code to detect if Windows XP is running inside Virtual PC 2007
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"

#define CONTEXT_ALL 0x1003F

int dummy(int);
unsigned long gf=0;
int __cdecl Handler(EXCEPTION_RECORD* pRec,void* est,unsigned char* pContext,void* disp)
{
	if(pRec->ExceptionCode==0xC0000096)  //Privileged instruction
	{
		//---------------------Installing the trick--------------------------------------
		*(unsigned long*)(pContext)=CONTEXT_ALL;/*CONTEXT_DEBUG_REGISTERS|CONTEXT_FULL*/
		*(unsigned long*)(pContext+0x4)=(unsigned long)(&dummy); 
		*(unsigned long*)(pContext+0x8)=(unsigned long)(&dummy);
		*(unsigned long*)(pContext+0xC)=(unsigned long)(&dummy);
		*(unsigned long*)(pContext+0x10)=(unsigned long)(&dummy);
		*(unsigned long*)(pContext+0x14)=0;
		*(unsigned long*)(pContext+0x18)=0x155; //Enable the four DRx On-Execute
		//---------------------------------------------------------------------------------
		(*(unsigned long*)(pContext+0xB8))++;
		return ExceptionContinueExecution;
	}
	else if(pRec->ExceptionCode==EXCEPTION_SINGLE_STEP)
	{
		if(gf==1)
		{
			   MessageBox(0,"Expected behavior (XP)","waliedassar",0);
			   ExitProcess(0);
		}
		gf++;
		(*(unsigned long*)(pContext+0xC0))|=0x00010000; //Set the RF (Resume Flag)
		return ExceptionContinueExecution;
	}
	return ExceptionContinueSearch;
}

int dummy(int x)
{
	x+=0x100;
	return x;
}

int main(int shitArg)
{
	unsigned long ver_=GetVersion();
	unsigned long major=ver_&0xFF;
	unsigned long minor=(ver_>>0x8)&0xFF;
	if(major==0x05 & minor==0x01) //Windows XP
	{
	    unsigned long x=0;
	    __asm
		{
		   push offset Handler
		   push dword ptr fs:[0x0]
           mov dword ptr fs:[0x0],esp
		   STI; Triggers an exception(privileged instruction)
		}
	    dummy(0xFF);
		__asm
		{
			pop dword ptr fs:[0x0]
			pop ebx
		}
		MessageBox(0,"Virtual PC 2007 detected (XP)","waliedassar",0);
	}
	return 0;
}
