//Source: http://pastebin.com/xhFABpPL
//http://waleedassar.blogspot.com/   (@waleedassar)
//Using the "WNetGetProviderName" function to detect VirtualBox.
//Thanks @deesse_k for the idea

#include "stdafx.h"
#include "windows.h"
#include "Winnetwk.h"

#pragma comment(lib,"MPR")

int main(void)
{
	   //method 9
       unsigned long pnsize=0x1000;
       char* provider=(char*)LocalAlloc(LMEM_ZEROINIT,pnsize);
       int retv=WNetGetProviderName(WNNC_NET_RDR2SAMPLE,provider,&pnsize);
       if(retv==NO_ERROR)
       {
                if(lstrcmpi(provider,"VirtualBox Shared Folders")==0)
                {
                        MessageBox(0,"VirtualBox detected","waliedassar",0);
                        ExitProcess(9);
                }
       }
       return 0;
}
