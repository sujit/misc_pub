//Source: http://pastebin.com/wuqcUaiE
//http://waleedassar.blogspot.com (@waleedassar)
//If running inside VirtualPC, the Illegal Instruction exception will be swallowed and no exception is raised.
// In this code "\x0f\x3F\x07\x0B" is used, other "\x0F\x3F\xXX\xXX" are also working.
//For more: http://pastebin.com/VDDRcmdL
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"

bool x=false;

int __cdecl Handler(EXCEPTION_RECORD* pRec,void* est,unsigned char* pContext,void* disp)
{
	  x=true;
	  (*(unsigned long*)(pContext+0xB8))+=4;
	  return ExceptionContinueExecution;
}

int main(int argc, char* argv[])
{
	__asm
	{
		push offset Handler
		push dword ptr fs:[0x0]
		mov dword ptr fs:[0x0],esp
		__emit 0Fh
        __emit 3Fh
        __emit 07h
        __emit 0Bh
	}
	if(x==false)
	{
		MessageBox(0,"VirtualPC detected","waliedassar",0);
		ExitProcess(0);
	}

	__asm
	{
		pop dword ptr fs:[0x0]
		pop eax
	}
	return 0;
}
