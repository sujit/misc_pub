// Source: http://pastebin.com/HVActZMC
//http://waleedassar.blogspot.com (@waleedassar)
//A method to detect VirtualPC
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"

int __cdecl Handler(EXCEPTION_RECORD* pRec,void* est,unsigned char* pContext,void* disp)
{
	if(pRec->ExceptionCode==EXCEPTION_SINGLE_STEP)
	{
		return ExceptionContinueExecution;
	}
	return ExceptionContinueSearch;
}


int main(int argc, char* argv[])
{
	unsigned long x=0;
	__asm
	{
		push offset Handler
		push dword ptr fs:[0x0]
                mov dword ptr fs:[0x0],esp

		pushad
		xor eax,eax
		xor ecx,ecx
		xor edx,edx
		xor ebx,ebx
		pushfd
		pop esi
		or esi,0x100 ;Trap flag
		push esi
		popfd
		CPUID
		pushfd
		pop eax
		mov x,eax
                popad
		pop dword ptr fs:[0x0]
		pop eax
    }
    if(x&0x100)
    {
        MessageBox(0,"Virtual Machine detected","waliedassar",0);
        ExitProcess(3);
    }
    return 0;
}
