// Source: http://pastebin.com/exAK5XQx
//http://waleedassar.blogspot.com (@waleedassar)
//Executing "\x0F\xC7\xC8\x05\x00" in VirtualPC 2007 triggers a reset error.
#include "stdafx.h"
#include "windows.h"
#include "stdio.h"

bool flag=false;

int __cdecl Handler(EXCEPTION_RECORD* pRec,void* est,unsigned char* pContext,void* disp)
{
	if(pRec->ExceptionCode==0xC000001D  || pRec->ExceptionCode==0xC000001E || pRec->ExceptionCode==0xC0000005)
	{
		flag=true;
	    (*(unsigned long*)(pContext+0xB8))+=5;
	    return ExceptionContinueExecution;
	}
	return ExceptionContinueSearch;
}



int main(int argc, char* argv[])
{
	__asm
	{
		push offset Handler
		push dword ptr fs:[0x0]
		mov dword ptr fs:[0x0],esp
	}
	flag=false;
	__asm
	{
		__emit 0x0F
		__emit 0xC7
		__emit 0xC8
		__emit 0x05
		__emit 0x00
	}
	if(flag==false)
	{
	      MessageBox(0,"VirtualPC detected","waliedassar",0);
	}
	__asm
	{
		pop dword ptr fs:[0x0]
		pop eax
	}
	return 0;
}
